"""Data structures used to collect data on how a setting of throttle
influences the acceleration given the current speed.
"""

class AccelerationTable(object):
    """A one-to-one table mapping velocities to ticks."""

    def __init__(self, throttle):
        """Create an instance of AccelerationTable given the fixed
        throttle setting.
        """

        self.throttle = throttle
        self.data = []
        self.sub_data = []
        self.new = False

    def add_data(self, velocity):
        """Add a data point to the table."""

        if (self.new == True):
            self.sub_data = []
            self.sub_data.append(velocity)
            self.data.append(self.sub_data)
            self.new = False
        else:
            self.sub_data.append(velocity)
            self.data[len(self.data) - 1] = self.sub_data

    def start_measurement(self):
        self.new = True

    def get_velocity(self, tick):
        """Get the velocity corresponding to the given tick."""

        minvel = -1
        for sub in self.data:
            if len(sub) > tick:
                if sub[tick] < minvel or minvel == -1:
                    minvel = sub[tick]
        return minvel

    def _find_closest(self, array, velocity):
        """Find the closest speed and tick within specific subarray of
        velocities
        """

        closest_speed = -1
        best_tick = -1

        for i in xrange(len(array) - 1):
            if array[i] <= velocity and array[i + 1] > velocity:
                if closest_speed == -1 or closest_speed >= array[i]:
                    closest_speed = array[i]
                    best_tick = i

        return [closest_speed, best_tick]

    def _get_tick(self, velocity, index):
        """Find tick of the closest possible velocity"""

        sub_index = index
        best_speed = -1
        best_tick = -1

        if index == -1:
            for sub in self.data:
                [cur_speed, cur_tick] = self._find_closest(sub, velocity)
                if best_speed == -1 or cur_speed < best_speed:
                    best_speed = cur_speed
                    best_tick = cur_tick
                    sub_index = self.data.index(sub)
        else:
            [best_speed, best_tick] = self._find_closest(
                                            self.data[index], velocity)

        return [best_tick, sub_index]

    def get_tick(self, velocity):
        """Get the tick corresponding to the given velocity."""

        [t, i] = self._get_tick(velocity, -1)
        return t

    def get_time(self, v1, v2):
        """Get the time required to accelerate/decelerate from <v1> to <v2>."""

        [t1, i1] = self._get_tick(v1, -1)
        [t2, i2] = self._get_tick(v2, i1)
        return t2 - t1

    def avg_velocity(self, v1, v2):
        """Compute the average velocity when accelerating/decelerating from
        v1 to v2.
        """

        [t1, i1] = self._get_tick(v1, -1)
        [t2, i2] = self._get_tick(v2, i1)
        sum_vel = 0
        for j in xrange(i1, i2 + 1):
            sum_vel += self.data[i1][j]

        return sum_vel / (i2 + 1 - i1)

    def expected_distance(self, v1, v2):
        """Compute the expected distance travelled when
        accelerating/decelerating from v1 to v2.
        """

        [t1, i1] = self._get_tick(v1, -1)
        [t2, i2] = self._get_tick(v2, i1)

        return self.avg_velocity(v1, v2) * (i2 - i1)
