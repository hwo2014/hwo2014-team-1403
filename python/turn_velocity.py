"""Algorithms for estimating the optimal velocity on track turns."""

from piece import Piece

class TurnVelocity(object):
    """This class is used to make decisions about the current setting of
    throttle when the car is driving along a turn piece.

    It can also be used to monitor the velocity of other cars, and if
    they don't fly off the track, we can use their velocity. Otherwise,
    if they do fly off, then we would know the critical velocity.
    """

    # ------------------------------------------------------------------
    # Here is my idea:
    #
    # Once we're on a turn piece, start a new measurement: that is,
    # remember the current tick etc. Start feeding this class with
    # position data so that we can keep track of the current velocity.
    # the <get_throttle> method will return 1.0 to tell the bot to
    # increase the velocity (this can be adjusted if needed). The
    # current slip angle can also be recorded just in case.
    #
    # When we crash, the <crashed> method should be called so that
    # we know that we've went over the optimal velocity on the last tick.
    # At this point we record the optimal turn constant and set the
    # boolean flag <turn_const_computed> to True.
    #
    # Next time we're on a turn, start a new measurement, feed the
    # class with position data, and ask for the current throttle
    # setting. The class should see that the turn constant is computed,
    # and use the <self.c> value to determine the optimal velocity, and
    # return the appropriate throttle value so that the car velocity
    # reaches and stays approximately at that velocity.
    #
    # IMPORTANT: since the velocity will likely go over the optimal
    # one at some point, we need to aim somewhere below the optimal
    # value, to account for the error (otherwise we might fly off the track).
    # ------------------------------------------------------------------

    # This is the turn constant, used to determine the optimal velocity
    # on a turn piece:
    #        v_max = C * radius
    #   where radius is the radius of the curvature of the turn
    #   (this value also depends on the lane the car is driving along).
    INIT_C = 1  # FIXME need some good initial value.

    # This value defines the amount of deviation from the optimal (critical)
    # velocity we are likely to be able to tolerate.
    # The critical velocity is the velocity at which we _might_ fly off
    # the track. Since we cannot be sure that we'll go at a constant
    # speed, we need to try to maintain a velocity just below the
    # critical one.
    #
    # We will aim to keep at velocity
    #
    #     v_t = (1 - ERROR_TOLERANCE) * v_max
    #
    # where v_max is the critical velocity.
    ERROR_TOLERANCE = 0.1

    def __init__(self, track):
        """Initialize a TurnVelocity object.

        Inputs:
            track :  a Track object providing information on the current
                     track.
        """

        self.track = track
        self.c = TurnVelocity.INIT_C
        self.turn_const_computed = False

        self.cur_radius = None

        # NOT the current velocity, but last SAFE velocity.
        self.last_velocity = None
        # Same here.
        self.last_angle = None
        self.last_tick = -1

    def start_measurement(self, velocity, radius):
        """Start a new measurement. This method should be called when
        the car drives into a new turn (with a new radius).

        Inputs:
            velocity :  car velocity just before the turn.
            radius   :  the radius of the turn that we've entered.
        """
        self.last_velocity = velocity
        self.radius = radius

    def new_position(self, status, tick):
        """Use new position data to update the current velocity.

        Inputs:
            pos  :  a Position object, providing information on the current
                    position.
            tick :  the tick to which the position data is relevant.
        """

        if self.last_pos:
            self.last_velocity = status.velocity

            if not self.turn_const_computed:
                # Refine.
                self.c = self.last_velocity / self.radius

        self.last_tick = tick

    def get_throttle(self, piece, lane_ix):
        """Return the throttle setting to be used at the next tick.

        Inputs:
            piece   :  a TurnPiece object we're currently driving along.
            lane_ix :  the index of a lane we're currently driving along.

        Output:
            the throttle value in [0.0, 1.0].
        """

        if not self.turn_const_computed:
            return 1.0
        else:
            # Will try to maintain velocity just below the optimal.

            if self.last_velocity:
                target_velocity = self.get_velocity(piece, lane_ix)
                if self.last_velocity < target_velocity:
                    return 1.0
                else:
                    return 0.0
            else:
                # Just in case. Bot needs to make sure to feed us
                # position data using <new_position> method.
                return 0.0

    def get_velocity(self, piece, lane_ix):
        """Return the target velocity at the turn piece defined by
        <piece> if the car is going to be driven along the lane
        given by <lane_ix>.

        Inputs:
            piece   :  a TurnPiece object for which the optimal
                       velocity needs to be computed.
            lane_ix :  an integer specifying the lane along which the
                       car is going to be driven.

        Outputs:
            velocity :  the target velocity, in units per tick.
        """

        if piece.type != Piece.TURN:
            print("ERROR TurnVelocity::get_throttle() called not on "
                  "turn piece")
            return 1.0

        return (1 - TurnVelocity.ERROR_TOLERANCE) * \
               self.c * piece.get_radius(lane_ix)

    def crashed(self):
        """This method should be called when a car has crashed during the
        current measurement.
        """
        # When this method is called, we'll know that the last measured
        # velocity was optimal, and thus the final value for the
        # turn constant can be computed, and the boolean flag
        # <turn_const_computed> can be set to True.
        self.turn_const_computed = True
