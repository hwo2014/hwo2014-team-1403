"""This file contains abstractions for the racing track."""

from piece import StraightPiece, TurnPiece

class Track(object):
    """Data structure representing a racing track, with relevant methods."""

    def __init__(self, track):
        """Initialize the track data structure.

        Inputs:
            track :  part of a dictionary returned by the server GAMEINIT
                     message containing the track pieces and lanes.
        """

        pieces_arr = track['pieces']

        # Store each track piece as an object of the appropriate subclass
        # of the Piece class.
        self.pieces = []
        for p in pieces_arr:
            switch = 'switch' in p
            if 'length' in p:
                self.pieces.append(StraightPiece(p['length'], switch))
            else:
                self.pieces.append(TurnPiece(p['radius'], p['angle'],
                                             track['lanes'], switch))
        self.lanes = track['lanes']

    def piece(self, ix):
        if ix < 0 or ix >= len(self.pieces):
            return None
        return self.pieces[ix]

    def get_pieces(self, ix1, ix2, lap1, lap2):
        """Return the pieces with indices in range [start_ix, end_ix].

        Inputs:
            ix1  :  index of the first piece in the requested range.
                    Must be in range [0, len(self.pieces)-1].
            ix2  :  index of the last piece in the requested range.
                    Must be in range [0, len(self.pieces)-1].
            lap1 :  the lap number (>= 0) of the first piece.
            lap2 :  the lap number (>= 0 and >= lap1) of the final piece.

        Note:
            the first piece defined by (ix1, lap1) must come before
            the final piece defined by (ix2, lap2), or be equal to it.

        Outputs:
            list containing the requested pieces.
            None if any of the indices are not in the required range,
            or if the first piece comes later than the final piece.
        """

        if (ix1 < 0 or ix1 >= len(self.pieces) or
            ix2 < 0 or ix2 >= len(self.pieces) or
            lap1 > lap2):
            # Not in range.
            return None

        if lap1 < lap2:
            pieces = self.pieces[ix1:]
            full_laps = lap2 - lap1 - 1
            while full_laps > 0:
                pieces.extend(self.pieces[:])
                full_laps -= 1
            pieces.extend(self.pieces[:ix2+1])
        else:
            pieces = self.pieces[ix1:ix2+1]

        return pieces

    def next_pieces(self, cur_ix, n):
        """Get next <n> pieces relative to the current piece index <cur_ix>.

        Inputs:
            cur_ix :  index of the current position of the car in <pieces>.
            n      :  number of pieces to return.

        Outputs:
            list of <n> Piece objects, starting with the one at
            index <cur_ix> + 1.
            None if <cur_ix> is invalid.
        """

        if cur_ix < 0 or cur_ix >= len(self.pieces):
            print("ERROR: Track::next_pieces(): invalid cur_ix ({})"
                  .format(cur_ix))
            return None

        ix1 = cur_ix + 1
        if ix1 == len(self.pieces):
            ix1 = 0
        num_laps = (ix1 + n - 1) / len(self.pieces)
        ix2 = (ix1 + n - 1) % len(self.pieces)
        return self.get_pieces(ix1, ix2, 0, num_laps)

        # TODO remove this.
        '''
        start_ix = cur_ix + 1

        # Number of pieces until the end of <self.pieces>
        tail_pieces = len(self.pieces) - start_ix
        if n <= tail_pieces:
            # No wrap-around.
            return self.pieces[start_ix : start_ix+n]

        pieces = self.pieces[start_ix : start_ix+tail_pieces]
        pieces_left = n - tail_pieces

        # Need to wrap around until <n> pieces are copied into <pieces>
        while pieces_left > 0:
            if pieces_left > len(self.pieces):
                pieces.extend(self.pieces[:])
            else:
                pieces.extend(self.pieces[:pieces_left])
            pieces_left -= n

        return pieces
        '''
