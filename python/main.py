import socket
import sys
from bot import Bot

if __name__ == "__main__":
    if len(sys.argv) != 5:
        host = "senna.helloworldopen.com"
        port = "8091"
        name = "Onvog"
        key = "O3E33+IkNp451w"
        #print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
    print("Connecting with parameters:")
    print("host={0}, port={1}, bot name={2}, key={3}"
          .format(host, port, name, key))
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, int(port)))
    bot = Bot(s, name, key)
    bot.run()
