import json
from track import Track
from piece import Piece
from acceleration_table import AccelerationTable

class Bot(object):
    """The racing bot. Communicates with the server and makes decisions."""

    # States of the bot.

    STATE_NONE = 0  # FIXME remove this.
    # Accelerating on a straight piece (throttle = 1.0)
    STATE_ACC = 1
    # Decelerating on a straight piece (throttle = 0.0)
    STATE_DEC = 2
    # Maximizing the speed on a turn (until we fly off)
    STATE_TURN_MAXIMIZE = 3
    # Performing a turn while attempting to maintaining some speed.
    STATE_TURN = 4

    def __init__(self, socket, name, key):
        """Initialize the bot.

        Inputs:
            socket :  an open socket to communicate with the server (socket).
            name   :  name of the bot (string).
            key    :  bot key (string).
        """

        self.socket = socket
        self.name = name
        self.key = key
        self.state = Bot.STATE_NONE
        self.turn_const_computed = False

        self.straight = False # is on straight piece
        self.accel_table = AccelerationTable(1)
        self.decel_table = AccelerationTable(0)
        self.dist1 = 0
        self.dist2 = 0

    def run(self):
        """Run the bot. Execution starts here."""
        self.join()
        self.msg_loop()

    #-------------------------------------------------------------------
    # Communication utilities.

    def msg(self, msg_type, data):
        """Send a message of type <msg_type> with the supplied <data> to the
        server.

        Inputs:
            msg_type :  type of the message (string).
            data     :  the message data (any type).
        """
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        """Send a message <msg> throught the socket."""
        self.socket.sendall(msg + "\n")

    def join(self):
        """Send a join message to the server."""
        return self.msg("join", {"name": self.name,
                                  "key": self.key})

    def throttle(self, throttle):
        """Tell the server to adjust the throttle to the specified value."""
        self.msg("throttle", throttle)

    def ping(self):
        """Ping the server."""
        self.msg("ping", {})

    #-------------------------------------------------------------------
    # Event handlers.

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_game_init(self, data):
        print("GameInit")
        self.track = Track(data['race']['track'])

    def on_car_positions(self, data):
        curIndex = int(data[0]['piecePosition']['pieceIndex'])
        cur_piece = self.track.pieces[curIndex]
        cur_lap = 1 # todo

        if cur_lap == 1:
            if cur_piece.type != Piece.TURN:
                if self.straight == False:
                    self.accel_table.start_measurement()
                    self.decel_table.start_measurement()
                    self.straight = True
                    self.dist2 = data[0]['piecePosition']['inPieceDistance']
                else:
                    self.dist1 = self.dist2
                    self.dist2 = data[0]['piecePosition']['inPieceDistance']
                    if self.dist2 <= 0.7 * cur_piece.length:
                        self.accel_table.add_data(self.dist2 - self.dist1)
                        self.throttle(1.0)
                    else:
                        self.decel_table.add_data(self.dist2 - self.dist1) #todo
                        self.throttle(0.0)
            else:
                self.straight = False
        else:
            self.throttle(0.5)

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        """Main loop of the program. React to server messages."""

        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'gameInit': self.on_game_init,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()
