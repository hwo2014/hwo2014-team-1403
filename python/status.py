
class Status(object):
    """Bot status data to be passed around."""

    def __init__(self, tick):
        self.pos = None
        self.velocity = 0.0
        self.last_tick = 0

    def new_position(self, new_pos, tick):
        distance = self.pos.dist(new_pos)
        self.velocity = distance / (tick - self.last_tick)
        self.pos = new_pos
        self.last_tick = tick
