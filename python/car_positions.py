"""Interface for 'CARPOSITIONS' messages."""

class CarPositions(object):

    def __init__(self, data, our_name):
        """Initialize CarPositions object.

        Inputs:
            data :  list of dictionairies. One dictionary
                    per car, as per techincal specifications.
            name :  name of our car.
        """

        self.our_pos = None
        self.cars = []
        for car in data:
            if car['id']['name'] == our_name:
                self.our_pos = Position(car)
            else:
                self.cars.append(Position(car))

        if self.our_pos is None:
            print("Could not find our car in 'carPositions' data.")

    def our_postion(self):
        return self.our_pos


class Position(object):

    def __init__(self, pos_data):
        self.name       = pos_data['id']['name']
        self.color      = pos_data['id']['color']
        self.angle      = pos_data['angle']
        self.piece_ix   = pos_data['piecePosition']['pieceIndex']
        self.piece_dist = pos_data['piecePosition']['inPieceDistance']
        self.lane       = (pos_data['piecePosition']['lane']['startLaneIndex'],
                           pos_data['piecePosition']['lane']['endLaneIndex'])
        self.lap        = pos_data['piecePosition']['lap']

    def __lt__(self, other_pos):
        """Return True if <self> comes before <other_pos>."""

        assert type(other_pos) is Position, \
               "Comparing Position object with a non-Position object"

        if self.lap == other_pos.lap:
            if self.piece_ix == other_pos.piece_ix:
                return self.piece_dist < other_pos.piece_dist
            else:
                return self.piece_ix < other_pos.piece_ix
        else:
            return self.lap < other_pos.lap

    def dist(self, other_pos, track):
        """Distance between this position and <other_pos>.

        Inputs:
            other_pos :  another Position object.
            track     :  Track object providing information on the current
                         track.

        Outputs:
            distance between this position and <other_pos> in units.
            If this position comes before <other_pos>, then the result
            is positive. Otherwise it is negative.
        """

        # Determine which one comes before.
        if self < other_pos:
            before = self
            after = other_pos
        else:
            before = other_pos
            after = self

        # Pieces between (and including) <before> and <after>.
        pieces = track.get_pieces(before.piece_ix, after.piece_ix,
                                  before.lap, after.lap)

        if pieces is None:
            print("ERROR: Position::dist(): could not get pieces.")
            return 0.0

        if len(pieces) == 1:
            # In the same piece.
            return after.piece_dist - before.piece_dist

        # We don't care about switching, the error shouldn't be too large.

        # This is the distance between the <before> position and the
        # end of its piece.
        dist1 = float(pieces[0].length(before.lane[0])) - before.piece_dist
        dist2 = after.piece_dist

        # Total length of pieces between pieces[0] and pieces[len(pieces)-1].
        dist_between = 0.0
        for piece in pieces[1:len(pieces)-1]:
            # We're assuming that if the car is switching to some lane
            # in the <before> position, it will stay there until
            # it reaches the <after> position, so the distance is
            # calculated accordingly.
            dist_between += piece.length(before.lane[1])

        dist = dist1 + dist_between + dist2
        if before is self:
            return dist
        else:
            return -dist
