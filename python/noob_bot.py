import json

class NoobBot(object):
    """The racing bot. Communicates with the server and makes decisions."""

    def __init__(self, socket, name, key):
        """Initialize the bot.

        Inputs:
            socket :  an open socket to communicate with the server (socket).
            name   :  name of the bot (string).
            key    :  bot key (string).
        """

        self._socket = socket
        self._name = name
        self._key = key

    def run(self):
        """Run the bot. Execution starts here."""
        self._join()
        self._msg_loop()

    #-------------------------------------------------------------------
    # Communication utilities.

    def _msg(self, msg_type, data):
        """Send a message of type <msg_type> with the supplied <data> to the
        server.

        Inputs:
            msg_type :  type of the message (string).
            data     :  the message data (any type).
        """
        self._send(json.dumps({"msgType": msg_type, "data": data}))

    def _send(self, msg):
        """Send a message <msg> throught the socket."""
        self._socket.sendall(msg + "\n")

    def _join(self):
        """Send a join message to the server."""
        return self._msg("join", {"name": self._name,
                                  "key": self._key})

    def _throttle(self, throttle):
        """Tell the server to adjust the throttle to the specified value."""
        self._msg("throttle", throttle)

    def _ping(self):
        """Ping the server."""
        self._msg("ping", {})

    #-------------------------------------------------------------------
    # Event handlers.

    def _on_join(self, data):
        print("Joined")
        self._ping()

    def _on_game_start(self, data):
        print("Race started")
        self._ping()

    def _on_game_init(self, data):
        print("GameInit")

    def _on_car_positions(self, data):
        self._throttle(0.62)

    def _on_crash(self, data):
        print("Someone crashed")
        self._ping()

    def _on_game_end(self, data):
        print("Race ended")
        self._ping()

    def _on_error(self, data):
        print("Error: {0}".format(data))
        self._ping()

    def _msg_loop(self):
        """Main loop of the program. React to server messages."""

        msg_map = {
            'join': self._on_join,
            'gameStart': self._on_game_start,
            'gameInit': self._on_game_init,
            'carPositions': self._on_car_positions,
            'crash': self._on_crash,
            'gameEnd': self._on_game_end,
            'error': self._on_error,
        }
        socket_file = self._socket.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self._ping()
            line = socket_file.readline()
