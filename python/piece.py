"""This file contains abstractions for track pieces."""

import math

class Piece(object):
    """Generic track piece. Only subclasses should be instantiated."""

    # Piece types.
    STRAIGHT = 0
    TURN = 1

    def __init__(self, t, switch):
        """Initialize a piece of the specified type <t>"""
        self.type = t
        self.switch = switch

    def length(self, lane_ix):
        raise NotImplementedError("Should be called from subclasses.")


class StraightPiece(Piece):
    """Straight track piece with a known length."""

    def __init__(self, length, switch):
        """Create a straight track piece with the specified length."""
        super(StraightPiece, self).__init__(Piece.STRAIGHT, switch)
        self.length = length

    def length(self, lane_ix):
        # Lane index does not matter.
        return self.length


class TurnPiece(Piece):
    """Track piece that is part of a turn. Determined by the turn radius
    and angle.
    """

    def __init__(self, radius, angle, lanes, switch):
        """Create a trun track piece.

        Inputs:
            radius :  the radius of the turn.
            angle  :  the angle of the turn, in degrees.
        """

        super(TurnPiece, self).__init__(Piece.TURN, switch)
        self.radius = radius
        self.angle = angle

        self.lanes = {}
        for lane in lanes:
            self.lanes[lane['index']] = lane['distanceFromCenter']

    def length(self, lane_ix):
        if lane_ix not in self.lanes.keys():
            print("ERROR: TurnPiece::length(): invalid lane index.")
            return 0.0
        return (self.angle * math.pi * (self.radius + self.lanes[lane_ix])
                / 180.0)

    def get_radius(self, lane_ix):
        """Return the bend radius at the lane defined by <lane_ix>."""
        return self.radius + self.lanes[lane_ix]
