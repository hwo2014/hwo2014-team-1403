"""Algorithms controlling the usage of the turbocharger."""

class Turbo(object):

    def __init__(self, track):
        self.track = track
        self.available = False
        self.duration_ms = 0.0
        self.duration_ticks = 0
        self.factor = 0.0

    def turbo_available(self, data):
        """Record the information regarding the currently available turbo."""

        self.available = True
        self.duration_ms = data['turboDurationMilliseconds']
        self.duration_ticks = data['turboDurationTicks']
        self.factor = data['turboFactor']

    def turbo_throttle(self, pos, acc_table, dec_table, turn_velocity):
        """Return the throttle to be used with the turbocharger.

        Inputs:
            pos           :  a Position object specifying the current
                             position on the track.
            acc_table     :  the acceleration table containing acceleration
                             data with throttle setting of 1.0.
            dec_table     :  the deceleration table with acceleration data
                             collected when using throttle setting of 0.0.
            turn_velocity :  a TurnVelocity object for our car.

        Outputs:
            the throttle value to be used with the turbocharger.
            If 0.0 is returned, then turbo should NOT be used.
        """

        # Need to use TurnVelocity to get the speed required for entering
        # the next turn, then estimate the distance we'll travel if
        # we used turbo right now, and see if we will be able to decelerate
        # fast enough to be able to enter the turn.
        # Seems hard to do without enough data. We might need to extrapolate
        # the acceleration and deceleration data because the velocities
        # achieved using turbo will likely be much higher than usual.

        # If it turns out to be the case that throttle * factor <= 1.0,
        # then it doesn't make sense to use the turbo. Just return 0.0.

        # Another note. If this is the last straight piece before the
        # finish line, use the turbo with throttle 1.0, no matter what.
        pass
